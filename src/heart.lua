Heart = {}

function Heart.load()
	Heart.fgfImg = love.graphics.newImage("img/heart_fg_f.png")
	Heart.fgbImg = love.graphics.newImage("img/heart_fg.png")
	Heart.bgImg = love.graphics.newImage("img/heart_bg.png")
	Heart.rate = 2
	Heart.time = 0
	Heart.scale = 1
	Heart.pulseMin = 1
	Heart.pulseAmp = 0.3

	Heart.heartsound = love.audio.newSource("snd/thenudo-heartbeat01.ogg","stream")
end

function Heart.update(dt)
	Heart.rate = 1.5 - Glass.percent * 1

	Heart.time = Heart.time + dt
	if (Heart.time > Heart.rate) then
		Heart.time = Heart.time - Heart.rate
		Heart.heartsound:stop()
		Heart.heartsound:play()
	end

	local x = Heart.time * 10
	local val = x * math.exp(-x)
	Heart.scale = Heart.pulseMin + val * Heart.pulseAmp
end

function Heart.drawBack()
	love.graphics.draw(Heart.bgImg, VideoModes.width/2, VideoModes.height/2, 0, 1, 1, Heart.bgImg:getWidth()*0.5, Heart.bgImg:getHeight()*0.5)
	love.graphics.draw(Heart.fgbImg, VideoModes.width/2, VideoModes.height/2, 0, Heart.scale, Heart.scale, Heart.fgbImg:getWidth()*0.5, Heart.fgbImg:getHeight()*0.5)
end

function Heart.drawFront()
	love.graphics.draw(Heart.fgfImg, VideoModes.width/2, VideoModes.height/2, 0, Heart.scale, Heart.scale, Heart.fgfImg:getWidth()*0.5, Heart.fgfImg:getHeight()*0.5)
end
