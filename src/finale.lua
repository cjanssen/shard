Finale = {}

function Finale.load()
	Finale.str = ""
	Finale.refStr = ""
	Finale.lineCount = 1
	Finale.oldStr = ""
	Finale.oldRefStr = ""
	Finale.oldLineCount = 1
	Finale.percent = 0
	Finale.fading = 1
	Finale.timer = 0
	Finale.setScreen(1)
end

function Finale.setScreen(scr)
	Finale.screen = scr
	Finale.oldRefStr = Finale.refStr
	Finale.oldStr = Finale.str
	Finale.oldLineCount = Finale.lineCount
	Finale.percent = 0
	if Finale.screen == 1 then
		Finale.refStr = "Have you ever been in love? "
		Finale.str = "Have you ever been in love? \nHorrible isn't it? \nIt makes you so vulnerable."
		Finale.lineCount = 3
	elseif Finale.screen == 2 then
		Finale.refStr = "and it means someone can get inside you and mess you up."
		Finale.str = "It opens your chest and it opens up your heart \nand it means someone can get inside you and mess you up."
		Finale.lineCount = 2
	elseif Finale.screen == 3 then
		Finale.refStr = " wanders into your stupid life... You give them a piece of you."
		Finale.str = "You build up all these defenses. You build up a whole armor,\nfor years, so nothing can hurt you, then one stupid person,\nno different from any other stupid person,\nwanders into your stupid life... You give them a piece of you."
		Finale.lineCount = 4
	elseif Finale.screen == 4 then
		Finale.refStr = "They did something dumb one day, like kiss you or smile at you,"
		Finale.str = "They didn't ask for it. \nThey did something dumb one day, like kiss you or smile at you,\nand then your life isn't your own anymore."
		Finale.lineCount = 3
	elseif Finale.screen == 5 then
		Finale.refStr = "It eats you out and leaves you crying in the darkness,"
		Finale.str = "Love takes hostages. It gets inside you. \nIt eats you out and leaves you crying in the darkness,\nso simple a phrase like 'maybe we should be just friends' \nor 'how very perceptive' turns into a glass splinter \nworking its way into your heart.\nIt hurts."
		Finale.lineCount = 6
	elseif Finale.screen == 6 then
		Finale.refStr = "It's a soul-hurt, a body-hurt, a real gets-inside-you-and-rips-you-apart pain."
		Finale.str = "Not just in the imagination. Not just in the mind. \nIt's a soul-hurt, a body-hurt, a real gets-inside-you-and-rips-you-apart pain.\nNothing should be able to do that. Especially not love."
		Finale.lineCount = 3
	elseif Finale.screen == 7 then
		Finale.refStr = "I hate love."
		Finale.str = "I hate love."
		Finale.lineCount = 1
	elseif Finale.screen == 8 then
		Finale.refStr = "The character Rose Walker in The Sandman #65"
		Finale.str = "\n\n\n(The character Rose Walker in The Sandman #65\nwritten by Neil Gaiman)"
		Finale.lineCount = 5
	end
end

function Finale.update(dt)
	Finale.percent = increaseExponential(dt, Finale.percent, 0.97)
	if Finale.screen > 8 then
		Finale.timer = Finale.timer + dt
		if Finale.timer > 1.5 then
			Finale.fading = decreaseExponential(dt, Finale.fading, 0.99)
			game.music:setVolume(Finale.fading)
			if Finale.fading < 0.001 then
				game.load()
			end
		end
	end
end

quote = "Have you ever been in love? Horrible isn't it? It makes you so vulnerable. It opens your chest and it opens up your heart and it means someone can get inside you and mess you up. You build up all these defenses. You build up a whole armor, for years, so nothing can hurt you, then one stupid person, no different from any other stupid person, wanders into your stupid life... You give them a piece of you. They didn't ask for it. They did something dumb one day, like kiss you or smile at you, and then your life isn't your own anymore. Love takes hostages. It gets inside you. It eats you out and leaves you crying in the darkness, so simple a phrase like 'maybe we should be just friends' or 'how very perceptive' turns into a glass splinter working its way into your heart. It hurts. Not just in the imagination. Not just in the mind. It's a soul-hurt, a body-hurt, a real gets-inside-you-and-rips-you-apart pain. Nothing should be able to do that. Especially not love. I hate love."

function Finale.draw()
	love.graphics.setColor(0,0,0,(1-Finale.percent)*Finale.fading*255)
	love.graphics.print(Finale.oldStr, VideoModes.width * 0.5 - game.font:getWidth(Finale.oldRefStr)*0.5, VideoModes.height*0.5- game.font:getHeight()*Finale.oldLineCount*0.5)
	love.graphics.setColor(0,0,0,Finale.percent*Finale.fading*255)
	love.graphics.print(Finale.str, VideoModes.width * 0.5 - game.font:getWidth(Finale.refStr)*0.5, VideoModes.height*0.5- game.font:getHeight()*Finale.lineCount*0.5)
end

function Finale.mousePressed()
	if (Finale.percent > 0.98) then
		Finale.setScreen(Finale.screen+1)
	end
end

