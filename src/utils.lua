function math.sign( a )
	if a >= 0 then
		return 1
	else
		return -1
	end
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end

function increaseExponentialSigned(dt, var, amount)
	local s = math.sign(var)
	var = math.abs(var)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	var = var * s
	return var
end

function decreaseExponentialSigned(dt, var, amount)
	local s = math.sign(var)
	var = math.abs(var)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	var = var * s
	return var
end

function linearApprox(dt, cur, dest, spd)
	local diff = dest - cur
	if math.abs(diff) > 0 then
		local inc = math.sign(diff) * spd * dt
		if math.abs(diff) < math.abs(inc) then
			inc = diff
		end
		cur = cur + inc
	end	
	return cur
end

function mixColors(colorA, colorB, fraction)
	return { colorA[1] * fraction + colorB[1]*(1-fraction),
		colorA[2] * fraction + colorB[2]*(1-fraction),
		colorA[3] * fraction + colorB[3]*(1-fraction) }
end

function mixColorsRGB(colorA, colorB, fraction)
	local rA,gA,bA,aA = hsv2rgb(colorA[1], colorA[2], colorA[3], 255)
	local rB,gB,bB,aB = hsv2rgb(colorB[1], colorB[2], colorB[3], 255)
	local result = mixColors({rA,gA,bA}, {rB,gB,bB}, fraction)
	local h,s,v,a = rgb2hsv(result[1],result[2],result[3],255)
	return {h,s,v}
end

function hsl2rgb(h, s, l, a)
    if s<=0 then return l,l,l,a end
    h, s, l = h/360*6, s/255, l/255
    local c = (1-math.abs(2*l-1))*s
    local x = (1-math.abs(h%2-1))*c
    local m,r,g,b = (l-.5*c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function hsv2rgb(h, s, v, a)
    if s <= 0 then return v,v,v,a end
    h, s, v = h/360*6, s/255, v/255
    local c = v*s
    local x = (1-math.abs((h%2)-1))*c
    local m,r,g,b = (v-c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end 
	return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function rgb2hsv(r, g, b, a)
	r,g,b = r/255, g/255, b/255
    local min = math.min(r,g,b)
	local max = math.max(r,g,b)

	local v = max
    local delta = max - min;
    if max > 0 then
        s = delta / max
	else 
		s = 0
		h = 0
		return 0,0,v*255,a
	end
	if r >= max then
		h = (g-b)/delta
	elseif g >= max then
		h = 2 + (b-r)/delta
	else
		h = 4 + (r-g)-delta
	end
    h = h * 60
    if h < 0 then
        h = h + 360
	end
    return h,s*255,v*255
end
