VideoModes = {}

function VideoModes.init()

	VideoModes.width = 1024
	VideoModes.height = 768
	VideoModes.fullscreen = 1

	VideoModes.list = love.graphics.getModes()
	
	local customList = {
  { width = 1920, height = 1440 },	
  { width = 1920, height = 1080 },
  { width = 1680, height = 1050 },
  { width = 1366, height = 768 },
  { width = 1280, height = 1024 },
  { width = 1280, height = 960 },
  { width = 1280, height = 720 },
  { width = 1024, height = 768 },
  { width = 800, height = 600 }
  }

	for i,v in ipairs(customList) do	
		table.insert(VideoModes.list, v)
	end
	table.sort(VideoModes.list, function(a, b) return a.width*a.height > b.width*b.height end)

	local n = table.getn(VideoModes.list)
	for i=1,n-1 do
		if VideoModes.list[n-i+1].width < 640 or VideoModes.list[n-i+1].width == VideoModes.list[n-i].width and VideoModes.list[n-i+1].height == VideoModes.list[n-i].height then
			table.remove(VideoModes.list, n-i+1)
		end
	end 

end
 
function VideoModes.getScreenDefaultOption(fullscreen)
	local fs = true
	if fullscreen == 0 then
		fs = false
	end

 	-- test from the list
	for i,v in ipairs(VideoModes.list) do
		if love.graphics.checkMode( v.width, v.height, fs ) then
			return v,i
		end
	end

	-- nothing!
	return nil
end

function VideoModes.findInitialMode()
	VideoModes.init()

	local validoption
	validoption, VideoModes.currentIndex = VideoModes.getScreenDefaultOption(VideoModes.fullscreen)
	if not validoption then
		VideoModes.fullscreen = 0
		validoption, VideoModes.currentIndex = VideoModes.getScreenDefaultOption(VideoModes.fullscreen)
	end
	if not validoption then
		print("ERROR: could not find a valid video mode")
		VideoModes.width = 0
		VideoModes.height = 0
		love.event.push("quit")
		return
	else
		VideoModes.width = validoption.width
		VideoModes.height = validoption.height
	end

	-- overwrite with the saved options
	VideoModes.loadOptions()
end

function VideoModes.nextMode()
	if VideoModes.currentIndex < table.getn(VideoModes.list) then
		VideoModes.currentIndex = VideoModes.currentIndex + 1
		VideoModes.width = VideoModes.list[VideoModes.currentIndex].width
		VideoModes.height = VideoModes.list[VideoModes.currentIndex].height
		VideoModes.reloadScreen()
	end
end

function VideoModes.prevMode()
	if VideoModes.currentIndex > 1 then
		VideoModes.currentIndex = VideoModes.currentIndex - 1
		VideoModes.width = VideoModes.list[VideoModes.currentIndex].width
		VideoModes.height = VideoModes.list[VideoModes.currentIndex].height
		VideoModes.reloadScreen()
	end
end

function VideoModes.toggleFullscreen()
	VideoModes.fullscreen = 1 - VideoModes.fullscreen
	VideoModes.reloadScreen()
end

function VideoModes.reloadScreen()
	if VideoModes.width == 0 then
		return
	end

	if game.screenWidth ~= VideoModes.width or game.screenHeight ~= VideoModes.height or game.fullscreen ~= VideoModes.fullscreen then
		local w = love.graphics.getWidth()
		local h = love.graphics.getHeight()
		local f = game.fullscreen

		local fullscreen = false
		if VideoModes.fullscreen ~= 0 then
			fullscreen = true
		end
		local success = love.graphics.checkMode( VideoModes.width, VideoModes.height, fullscreen ) and
						love.graphics.setMode( VideoModes.width, VideoModes.height, fullscreen )	
		if success then
			game.screenWidth = VideoModes.width
			game.screenHeight = VideoModes.height
			game.fullscreen = VideoModes.fullscreen
		else
			VideoModes.width = w
			VideoModes.height = h
			VideoModes.fullscreen = f
			fullscreen = false
			if VideoModes.fullscreen ~= 0 then
				fullscreen = true
			end
			love.graphics.setMode(w,h,fullscreen)
		end
		VideoModes.saveOptions()
	end
end
-----------------------------------------------------------
-- FILE I/O
function VideoModes.loadOptions()
	VideoModes.filename = "shard-options.txt"
	if not love.filesystem.exists(VideoModes.filename) then
		return
	end

	local contents,size = love.filesystem.read(VideoModes.filename)
	if size ~= 0 then
		VideoModes.parseOptions(contents)
	end
end

function VideoModes.saveOptions()
	local str = VideoModes.serialize()
	love.filesystem.write(VideoModes.filename,str,str:len())
end

function VideoModes.parseOptions(str)
	VideoModes.currentIndex = VideoModes.readOption(str, "index", VideoModes.currentIndex)
	VideoModes.width = VideoModes.readOption(str, "width", VideoModes.width)
	VideoModes.height = VideoModes.readOption(str, "height", VideoModes.height)
	VideoModes.fullscreen = VideoModes.readOption(str, "fullscreen", VideoModes.fullscreen)
end

function VideoModes.readOption(str, optionString, defaultValue)
	local stBegin, stEnd = string.find(str, optionString)
	if not stBegin then
		return defaultValue
	end

	local valuePos, valueEnd = string.find(str, "%d+", stEnd)
	if not valuePos then
		return defaultValue
	end

	if string.match(string.sub(str, stEnd+1, valuePos-1), "%a+") then
		return defaultValue
	end

	return tonumber(string.sub(str,valuePos,valueEnd))
end

function VideoModes.serialize()
	local str = ""
	str = str.."index "..VideoModes.currentIndex.."\r\n"
	str = str.."width "..VideoModes.width.."\r\n"
	str = str.."height "..VideoModes.height.."\r\n"
	str = str.."fullscreen "..VideoModes.fullscreen.."\r\n"
	return str	
end

