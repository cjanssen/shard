game = {}

function game.load()
	VideoModes.reloadScreen()
	game.initfont()
	game.loadmusic()
	game.initvalues()
	Heart.load()
	Glass.load()
	Texts.load()
	Finale.load()
end

function game.initScreen()
	VideoModes.findInitialMode()
end

-- game modes
-- 1 . title
-- 2 . ingame
-- 3 . switch to end
-- 4 . end
-- 5 . options
function game.initvalues()
	game.setMode(1)
	game.overlayOp = 1

	game.titleOp = 0
	game.activeTitle = 1
	game.timer = 0
	game.aboutToStart = false
	game.thr1 = 4
	game.thr2 = 6
	game.thr3 = 8

end

function game.initfont()
	game.font = love.graphics.newFont("fnt/Scrogglet.ttf", 24)
	love.graphics.setFont(game.font)
end

function game.loadmusic()
	game.music = love.audio.newSource("snd/Theres_Probably_No_Time_Chris_Zabriskie.ogg", "stream")
	game.music:setLooping(true)
	game.music:play()
end

function game.update(dt)
	if not game.started then
		game.started = true
		dt = 0
	end
	if game.mode == 1 then
		game.updateTitle(dt)
	elseif game.mode == 2 then
		game.overlayOp = decreaseExponential(dt, game.overlayOp, 0.992)
		Heart.update(dt)
		Glass.update(dt)
		Texts.update(dt)
	elseif game.mode == 3 then
		game.overlayOp = increaseExponential(dt, game.overlayOp, 0.9)
		if game.overlayOp >= 1 then
			game.setMode(4)		
		end 
	elseif game.mode == 4 then
		Finale.update(dt)
	end
	
end

function game.updateTitle(dt)
	game.timer = game.timer + dt
	if game.timer < game.thr1 then
		game.activeTitle = 1
		game.titleOp = increaseExponential(dt, game.titleOp, 0.992)
	elseif game.timer < game.thr2 then
		game.titleOp = decreaseExponential(dt, game.titleOp, 0.97) 
	else
		game.activeTitle = 2
		if not game.aboutToStart then
			game.titleOp = increaseExponential(dt, game.titleOp, 0.98)
		else
			game.titleOp = decreaseExponential(dt, game.titleOp, 0.96)
			if game.titleOp < 0.05 then
				game.setMode(2)
			end
		end
	end
end

function game.draw()
	love.graphics.push()

	love.graphics.setColor(255,255,255)		
	love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())

	if game.mode == 1 then
		love.graphics.setColor(0,0,0,255*game.titleOp)
		if game.activeTitle == 1 then
			love.graphics.print("a game by Christiaan Janssen", math.floor(VideoModes.width - game.font:getWidth("a game by Christiaan Janssen") - 15), math.floor(VideoModes.height - game.font:getHeight() - 15))
		else
			love.graphics.print("shard", math.floor(VideoModes.width * 0.5 - game.font:getWidth("shard")*0.5), math.floor(VideoModes.height*0.5- game.font:getHeight()*0.5))
		end
	elseif game.mode == 2 or game.mode == 3 then
		Heart.drawBack()
		Glass.draw()
		Heart.drawFront()

		Texts.draw()

		love.graphics.setColor(255,255,255, game.overlayOp*255)
		love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())
	end
	if game.mode == 4 then
		Finale.draw()
	end

	love.graphics.pop()
end

function game.setMode(newMode)
	game.mode = newMode
end

function game.keypressed( key )
	if key == "escape" then
		love.event.push("quit")
		return
	end


	if key == "f2" then
		VideoModes.toggleFullscreen()
	end

	if key == "f3" then
		VideoModes.prevMode()
	end

	if key == "f4" then
		VideoModes.nextMode()
	end
end

function game.mousepressed(x, y, button)
	if game.mode == 1 and game.timer > game.thr3 then
		game.aboutToStart = true
	elseif game.mode == 2 then
	elseif game.mode == 4 then
		Finale.mousePressed()
	end
end


