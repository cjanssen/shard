Glass = {}

function Glass.load()
	Glass.img = love.graphics.newImage("img/glass.png")
	Glass.x = 900
	Glass.y = 0
	Glass.percent = 0
	Glass.percentGoal = 0
	Glass.time = 0
end

function Glass.update(dt)
	if game.mode == 2 then
		if Glass.percent >= 1 then
			game.setMode(3)
		end
		Glass.time = Glass.time + dt
		if Glass.percent < Glass.percentGoal then
			Glass.percent = Glass.percent + dt * 0.1
		end
		if Glass.percent > Glass.percentGoal then
			Glass.percent = Glass.percentGoal
		end
		Glass.x = VideoModes.width * 0.85 - Glass.percent * VideoModes.width * 0.3
		Glass.y = VideoModes.height * 0.5 * Glass.percent
	end
end

function Glass.draw()
	love.graphics.draw(Glass.img, Glass.x, Glass.y, math.pi*0.13, 1, 1, Glass.img:getWidth()*0.5, Glass.img:getHeight()*0.5)
end
