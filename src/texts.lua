Texts = {}

function Texts.load()
	Texts.mouseX = 0
	Texts.mouseY = 0
	Texts.mouseDown = false

	Texts.colors = {
		default = {64, 64, 64},
		hovered = {0,0,128},
		disabled = {192,192,192},
		selected = {0,0,255}
	}
	Texts.initUI()
	Texts.heartbox = {
		x = VideoModes.width/2 - Heart.bgImg:getWidth() * 0.5,
		y = VideoModes.height/2 - Heart.bgImg:getHeight() * 0.5,
		w = Heart.bgImg:getWidth(),
		h = Heart.bgImg:getHeight()
	}
	Texts.deletesomething = false
end

function Texts.update(dt)
	if game.mode == 2 and Glass.percentGoal < 1 then
		Texts.checkGenerated(dt)
		Texts.updateAllUI(dt)
	end
end

function Texts.regenerateStrings()
	for i,v in ipairs(Texts.originalStrings) do
		table.insert(Texts.strings, v)
	end
end

function Texts.removeString(str)
	for i=1,table.getn(Texts.originalStrings) do
		if Texts.originalStrings[i][1] == str then
			table.remove(Texts.originalStrings, i)
			return
		end
	end
end

function Texts.checkGenerated(dt)
	if table.getn(Texts.currentTexts) == 0 then
		if table.getn(Texts.strings) < 3 then
			Texts.strings = {}
			Texts.regenerateStrings()
		end

		local count = math.min(3, table.getn(Texts.strings))
		local localStrings = {}
		for i=1,count do
			local index = math.random(table.getn(Texts.strings))
			table.insert(Texts.currentTexts, Texts.strings[index])
			table.remove(Texts.strings, index)
			if table.getn(Texts.strings) == 0 then
				break
			end
		end
		for i,v in ipairs(Texts.currentTexts) do
			local collided = true
			local pos = { math.random(VideoModes.width), math.random(VideoModes.height) }
			Texts.newButton(v[1], pos[1], pos[2], i, v[2])
			local cnt = table.getn(Texts.UIlist)
			local iters = 300
			while collided do
				-- not offscreen
				collided = false
				if Texts.UIlist[cnt].x + Texts.UIlist[cnt].w > VideoModes.width or
					Texts.UIlist[cnt].y + Texts.UIlist[cnt].h > VideoModes.height then
					collided = true
				else
					for j = 1, cnt-1 do
						if Texts.collides(Texts.UIlist[cnt], Texts.UIlist[j]) then
							collided = true
						end
					end
				end
				if collided then
					Texts.UIlist[cnt].x = math.random(VideoModes.width - Texts.UIlist[cnt].w)
					Texts.UIlist[cnt].y = math.random(VideoModes.height - Texts.UIlist[cnt].h)
					iters = iters - 1
					if iters == 0 then
						collided = false
					end
				end
			end
		end
	end
end

function Texts.updateAllUI(dt)
	Texts.mouseX = love.mouse.getX()
	Texts.mouseY = love.mouse.getY()
	local mouseDown = love.mouse.isDown("l")
	for i,v in ipairs(Texts.UIlist) do
		if mouseDown then
			Texts.clickedUI(v, Texts.mouseX, Texts.mouseY)
		else
			Texts.mouseoverUI(v, Texts.mouseX, Texts.mouseY)
		end
		Texts.updateUI(v, dt)
	end
	Texts.mouseDown = mouseDown

	if Texts.deletesomething then
		local cnt = table.getn(Texts.UIlist)
		for i=1,cnt do
			local index = cnt + 1 - i
			if Texts.UIlist[index].todelete then
				table.remove(Texts.UIlist, index)
			end
		end
	end
end

function Texts.draw()
	for i,v in ipairs(Texts.UIlist) do
		Texts.drawUI(v)
	end
end


-----------------------------------------------------------------

function Texts.newButton(title_, x_, y_, optionid_, value_)
	table.insert(Texts.UIlist, {
		title = title_,
		x = x_,
		y = y_,
		w = game.font:getWidth(title_),
		h = game.font:getHeight(),
		optionid = optionid_,
		value = value_,
		selected = false,
		hovered = false,
		currentColor = Texts.colors.default,
		todelete = false,

		currentX = VideoModes.width * 0.5,
		currentY = VideoModes.height * 0.5,
		opacity = 0,
		shadeFactor = 0
	})
end


function Texts.collides(elem1, elem2)
	if elem1.x > elem2.x + elem2.w then
		return false
	end
	if elem1.x + elem1.w < elem2.x then
		return false
	end
	if elem1.y > elem2.y + elem2.h then
		return false
	end
	if elem1.y + elem1.h < elem2.y then
		return false
	end
	return true
end

function Texts.updateUI(elem, dt)
	-- active?
	if elem.selected then
		elem.currentColor = Texts.colors.selected
	elseif elem.hovered then
		elem.currentColor = Texts.colors.hovered
	else
		elem.currentColor = Texts.colors.default
	end

	if elem.shadeFactor < 1 then
		elem.shadeFactor = increaseExponential(dt, elem.shadeFactor, 0.98)
		local cw = (VideoModes.width - elem.w)*0.5
		local ch = (VideoModes.height - elem.h)*0.5
		elem.currentX =  cw + (elem.x - cw) * elem.shadeFactor
		elem.currentY = ch + (elem.y - ch) * elem.shadeFactor
		elem.opacity = elem.shadeFactor
	end

end

function Texts.drawUI(elem)
	love.graphics.setColor(elem.currentColor[1], elem.currentColor[2], elem.currentColor[3], elem.opacity*255)
	love.graphics.print(elem.title, elem.currentX, elem.currentY)
end

function Texts.mouseoverUI(elem, x, y)
	if elem.uitype == 1 then
		return
	end

	elem.hovered = false
	if x >= elem.x and x <= elem.x + elem.w and y >= elem.y and y <= elem.y + elem.h then
		elem.hovered = true
	end

	if elem.uitype == 3 and elem.dragging then
		elem.dragging = false
	end
end

function Texts.clickedUI(elem, x, y)
	local inarea = false
	if x >= elem.x and x <= elem.x + elem.w and y >= elem.y and y <= elem.y + elem.h then
		inarea = true
	end

	if not Texts.mouseDown then
		if inarea and not elem.selected then
			elem.selected = true
			Texts.removeString(elem.title)
			Texts.deleteNonselected()
			Glass.percentGoal = Glass.percentGoal + elem.value
		end
	end
end

function Texts.deleteNonselected()
	for i,v in ipairs(Texts.UIlist) do
		if not v.selected then
			v.todelete = true
		end
	end
	Texts.deletesomething = true
	Texts.currentTexts = {}
end

function Texts.initUI()
	Texts.UIlist = {}
	Texts.currentTexts = {}
	Texts.strings = {}
	Texts.originalStrings = {
		{ "I care for you", 0.1 },
		{ "I like the way you always laugh", 0.05 },
		{ "You don't have to worry for me", 0.02 },
		{ "I have a lot of fun around you", 0.2 },
		{ "This cake was delicious!", 0.06 },
		{ "That's an ugly pair of shoes", 0.05 },
		{ "I only want to be friends", 0.3 },
		{ "I would like to find someone like you", 0.3 },
		{ "You're so funny", 0.1 },
		{ "You remind me of my ex", 0.15 },
		{ "Does this look good on me?", 0.06 },
		{ "You're cute", 0.1 },
		{ "You always have something fun to say", 0.05 },
		{ "I need to be alone for a while", 0.08 },
		{ "Sorry that I didn't call yesterday", 0.02 },
		{ "I feel attracted to idiots like that", 0.15 },
		{ "Tomorrow is your birthday!", 0.01 },
		{ "We can watch a movie together", 0.01 },
		{ "I feel so lonely sometimes", 0.02 },
		{ "Maybe I will never find love", 0.2 },
		{ "You should find someone", 0.1 },
		{ "You're my best friend", 0.3 },
		{ "Sorry, I won't have time tomorrow", 0.08 },
		{ "That book you lent me was great", 0.01 },
		{ "Are you in love with anyone?", 0.05 },
		{ "You know so many things!", 0.15 },
		{ "I'm not prepared", 0.2 },
		{ "I know, but that asshole is so hot", 0.2 },
		{ "I'm watching a movie with my ex tonight", 0.15 },
		{ "No one loves me", 0.1 },
		{ "You kept that side of yourself secret", 0.17 },
		{ "You always know how to suprise me", 0.05 },
		
	}
	Texts.slots = {}
	for i=1,math.floor(VideoModes.height/25)-1 do
		table.insert(Texts.slots, { 10, i*24 } )
		table.insert(Texts.slots, { 700, i*24 } )
	end
end



